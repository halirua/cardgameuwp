﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CardGameProject
{
    class CardDeck
    {
        /// <summary>
        /// The list of cards that are in the deck to be played
        /// (read as "List of Cards")
        /// </summary>
        private List<Card> _cardList;

        /// <summary>
        /// Constructor for CardDeck objects
        /// </summary>
        public CardDeck()
        {
            _cardList = new List<Card>();
        }
    }
}

