﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CardGameProject
{
    /// <summary>
    /// Represents the card suit with the values diamond, heart, club, spade
    /// </summary>
    enum CardSuit
    {
        Diamonds = 1,
        Clubs,
        Hearts,
        Spades
    }
    /// <summary>
    /// Represents a card in a card game
    /// </summary>
    class Card
    {
        /// <summary>
        /// The numeric value of the card
        /// </summary>
        private byte _value;

        /// <summary>
        /// The suit of the card
        /// </summary>
        private CardSuit _suit;

        public Card(byte value, CardSuit suit)
        {
            // initialize the value of the card
            _value = value;

            // initialize the suit of the card
            _suit = suit;
        }

        public byte GetValue()
        {
            return _value;
        }

        /// <summary>
        /// Changes the value of the card
        /// </summary>
        /// <param name="newValue">thew new value to be set</param>
        public void SetValue(byte newValue)
        {
            _value = newValue;
        }

        public CardSuit GetSuit()
        {
            return _suit;
        }

        public void SetSuit(CardSuit suit)
        {
            _suit = suit;
        }

        //TODO: declare GetCardName()
        public string GetCardName()
        {
            return null;
        }
        //TODO: decalre GetSuitName()
        public string GetSuitName()
        {
            return null;
        }
    }
}

