﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CardGameProject
{
    /// <summary>
    /// Represents the score of the game
    /// </summary>
    struct Score
    {
        /// <summary>
        /// The score value for the player
        /// </summary>
        private int _playerScore;

        /// <summary>
        /// The score value for the house
        /// </summary>
        private int _houseScore;
    }
    class CardGame
    {
        /// <summary>
        /// The card deck used in the game which has a list of cards inside
        /// </summary>
        private CardDeck _cardDeck;

        /// <summary>
        /// The current score of the game
        /// </summary>
        private Score _score;

        /// <summary>
        /// The card being played by the player in the current round
        /// </summary>
        private Card _playerCard;

        /// <summary>
        /// The card being played by the house in the current round
        /// </summary>
        private Card _houseCard;

        public CardGame()
        {
            // initialize the _cardDeck field variable
            _cardDeck = new CardDeck();

            // initialize the score of the game
            _score = new Score();

            // initialize the current player card
            _playerCard = null;

            // initialize the current house card
            _houseCard = null;
        }

        public void Play()
        {
            // declare the roundResult local variable
            sbyte roundResult = 0;
        }

        public sbyte PlayRound()
        {
            // declare the exchangeInput local variable
            string exchangeInput = "";

            byte playerCardRank = 0;

            byte houseCardRank = 0;

            return 0; //TODO: implement PlayRound();

        }
    }
}


